#include "opencv2/highgui/highgui.hpp"
#include "opencv/cv.h"
#include <math.h>
#include <stdio.h>
#include "Gabor.h"

using namespace cv;

Gabor::Gabor(){

}

void Gabor:: create_gabor_kernel_v1( float sig, float thet, float lm, float gamma , float ps , float bw){

    float    sigma=sig ;
    float    lmbda=lm ;
    float    slratio;
    float    N;
    int      kernel_size;

    slratio = (1/CV_PI)*sqrt(log(2.0)/2)*( (pow(2,bw)+1) /(pow(2,bw)-1));

    if  (sigma == 0)
        sigma  = slratio * lmbda;
    else if (lmbda == 0)
        lmbda = sigma / slratio;

    float    theta = (thet*CV_PI/180);
    float    psi   = (ps  *CV_PI/180);

    int      x = 0;
    int      y = 0;
    float    X = 0;
    float    Y = 0;

    if (gamma <= 1 && gamma > 0)
        N = cvRound(2.5*sigma/gamma);  //sigma/gamma = 1.6 so N=4
    else
        N = cvRound(2.5*sigma);

    kernel_size = (int)(2*N+1);

    kernel_real =cvCreateMat(kernel_size,kernel_size, CV_32FC1);
    kernel_imag =cvCreateMat(kernel_size,kernel_size, CV_32FC1);
    kernel_mag  =cvCreateMat(kernel_size,kernel_size, CV_32FC1);

    float    X2,Y2;
    float    sigma2;
    float    gamma2;
    float    F;

    float    real = 0;
    float    imag = 0;
    float    mag  = 0;


    float K;
    float temp1;
    float temp2;
    float temp3;

    int kernel_mid    = (int) (kernel_size-1)/2;     //kernel anchor

    sigma2=  sigma*sigma;
    gamma2=  gamma*gamma;

    K = (1/(2*(CV_PI)*sigma2));
    F = (2*CV_PI )/(float) lmbda ;          // Frequency

    printf("Sigma = %f ; Lmbda = %f , kernel_size = %d ",sigma,lmbda,kernel_size);

    for(int i=0;i<kernel_size;i++){   //row
        for(int j=0;j<kernel_size;j++){  //col
            x = j-(kernel_mid);        //col
            y = i-(kernel_mid);        //row

            X =    (x*cos(theta))   + ( y*sin(theta))  ;
            Y =    (-x*sin(theta))  + ( y*cos(theta))  ;

            Y2    =  Y*Y;
            X2    =  X*X;

            temp1=K*exp( -0.5* (  (X2+(Y2*gamma2)) / (sigma2) )  );

            temp2= cos(  ((X*F)+ psi)  );

            temp3= sin(  ((X*F)+ psi)  );

            real = (temp1*temp2);
            imag = (temp1*temp3);

            mag  = ((real*real)+(imag*imag));


            cvSetReal2D((CvMat*)kernel_real, i, j, real );
            cvSetReal2D((CvMat*)kernel_imag, i, j, imag );
            cvSetReal2D((CvMat*)kernel_mag , i, j,mag  );
        }
    }

}


















