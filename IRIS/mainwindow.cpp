#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>
#include <cstdlib>
#include <iostream>   // std::cout
#include <string>

RNG rng(12345);

vector<vector<Point> > contours;
vector<Vec4i> hierarchy;

using namespace std;
// Constructor member
MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    
    // Original Image and sub-images
    this->Image_Source = new QImage(200,200, QImage::Format_RGB888);
    
    this->Image_code2 = new QImage(720,60, QImage::Format_RGB888);
    
    // viewers
    viewer_original = new RCDraw(200,200, Image_Source, ui->viewer_original);
    viewer_code2 = new RCDraw(720,60, Image_code2, ui->viewer_code_2);
    
    // connect to button
    connect ( ui->Load_button, SIGNAL (clicked()), this, SLOT( Load_Image() ) );
}
// Destructor member
MainWindow::~MainWindow()
{
    delete ui;
    delete Image_Source;
    delete viewer_original;
}

void MainWindow::Load_Image()
{
    // Looking for a image name in a directory with browsing box
    QString Directory="capturas";
    QString fn = QFileDialog::getOpenFileName(this,"Choose a frame to download",Directory, "Images (*.png *.xpm *.jpg *.bmp)");
    
    src = cvLoadImage(fn.toStdString().c_str());
    Original_Image= imread(fn.toStdString(), CV_LOAD_IMAGE_COLOR);
    cv::resize(Original_Image, mat_original, Size(200,200), 0, 0, cv::INTER_CUBIC);
    memcpy(Image_Source->bits(),mat_original.data,mat_original.rows*mat_original.cols*sizeof(uchar)*3);
    viewer_original->update();
}
IplImage* MainWindow::DrawHistogram(CvHistogram *hist, float scaleX, float scaleY)
{
    float histMax = 0;
    cvGetMinMaxHistValue(hist, 0, &histMax, 0, 0);
    IplImage* imgHist = cvCreateImage(cvSize(256*scaleX, 64*scaleY), 8 ,1);
    cvZero(imgHist);
    for(int i=0;i<255;i++)
    {
        float histValue = cvQueryHistValue_1D(hist, i);
        float nextValue = cvQueryHistValue_1D(hist, i+1);
        
        CvPoint pt1 = cvPoint(i*scaleX, 64*scaleY);
        CvPoint pt2 = cvPoint(i*scaleX+scaleX, 64*scaleY);
        CvPoint pt3 = cvPoint(i*scaleX+scaleX, (64-nextValue*64/histMax)*scaleY);
        CvPoint pt4 = cvPoint(i*scaleX, (64-histValue*64/histMax)*scaleY);
        
        int numPts = 5;
        CvPoint pts[] = {pt1, pt2, pt3, pt4, pt1};
        
        cvFillConvexPoly(imgHist, pts, numPts, cvScalar(255));
    }
    return imgHist;
}

void MainWindow::Smooth(IplImage *src , IplImage *dst){
    cvCvtColor( src, dst, CV_RGB2GRAY );
    cvSmooth( dst, dst, CV_MEDIAN,9,9);
    
}
void MainWindow::process_image(){
    //Reset vars. If not, iris ROIs will overlap if we use this function more than once
    dst    = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 3 );
    
    grayscale    = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    
    smooth = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    pupil  = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    iris   = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    pedge  = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    iedge  = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    
    
    eyelid_mask  = cvCreateImage( cvGetSize(src), IPL_DEPTH_8U, 1 );
    iris_mask    = cvCreateImage(cvGetSize(src), 8, 1);
    mask         = cvCreateImage(cvGetSize(src), IPL_DEPTH_8U, 3 );
    res          = cvCreateImage(cvGetSize(src), 8, 3);
    
    //Preprocess
    
    cvCvtColor( src, grayscale, CV_RGB2GRAY );
    cvCopy(src,dst);
    Smooth(src,smooth);
    
    ///////////////////////////////////////////////////////
    //////////////////// PUPIL/////////////////////////////
    ///////////////////////////////////////////////////////
    
    int numBins = 256;
    float range[] = {0, 255};
    float *ranges[] = { range };
    
    CvHistogram *hist = cvCreateHist(1, &numBins, CV_HIST_ARRAY, ranges, 1);
    cvClearHist(hist);
    
    
    cvCalcHist(&smooth, hist, 0, 0);
    IplImage* imgHist = DrawHistogram(hist,1,1);
    cvClearHist(hist);
    
    cvThreshold(smooth,pupil,50,255,CV_THRESH_BINARY);
    
    cvCanny(pupil,pedge,40,50);
    
    //////////////////////////////////////////////////////////
    //////////////////////IRIS////////////////////////////////
    //////////////////////////////////////////////////////////
    
    cvThreshold(smooth,iris,100,255,CV_THRESH_BINARY); //115
    cvCanny(iris,iedge,1,255);
    
    /////////////////////////////////////////////////////////
    ///////////////////////Eyelids///////////////////////////
    /////////////////////////////////////////////////////////
    
    cvThreshold(smooth,eyelid_mask,150,255,CV_THRESH_OTSU);
    cvNot(eyelid_mask,eyelid_mask);
    
    CvMemStorage* storage_pupil = cvCreateMemStorage(0);
    
    CvSeq* presults = cvHoughCircles(pedge,storage_pupil,CV_HOUGH_GRADIENT,2,src->width,255,1);
    for( int i = 0; i < presults->total; i++ )
    {
        float* p = (float*) cvGetSeqElem( presults, i );
        CvPoint pt = cvPoint( cvRound( p[0] ),cvRound( p[1] ) );
        
        xp=cvRound( p[0] );
        yp=cvRound( p[1] );
        rp=p[2];
        
        cvCircle(dst,pt,cvRound( p[2] ),CV_RGB(0,255,255),1,400);
        
        
        xroi= xp-shift;
        yroi= yp-shift;
        
        cvRectangle(dst,cvPoint(( p[0] )-shift,p[1]-shift),cvPoint(( p[0] )+shift,p[1]+shift),CV_RGB(255,0,255), 1);
        
        CvRect roi= cvRect(xroi  ,yroi,shift*2,shift*2);
        
        cvSetImageROI( iedge, roi );
        
    }
    
    
    CvMemStorage* storage_iris = cvCreateMemStorage(0);
    
    CvSeq* iresults = cvHoughCircles(iedge,storage_iris,CV_HOUGH_GRADIENT,2,src->width,1,50,50);
    for( int i = 0; i < iresults->total; i++ )
    {
        float* p = (float*) cvGetSeqElem( iresults, i );
        
        CvPoint pt = cvPoint( cvRound( p[0] )+xroi,cvRound( p[1] )+yroi );
        cvCircle(dst,pt,cvRound( p[2] ),CV_RGB(255,0,0),1,400);
        
        xi=cvRound( p[0] )+xroi;
        yi=cvRound( p[1] )+yroi;
        ri=(p[2]);
        
        cvCircle(iris_mask,pt,cvRound( p[2] ),CV_RGB(255, 255, 255),-1, 8, 0);
        
    }
    
    
    cvResetImageROI(iedge);
    
    cvAnd(dst,dst,res,iris_mask);
    
    cvAnd(res,res, mask, eyelid_mask);
    
    /////////////////////////////////////////////////////////
    ///////////////////////SHOW RESULT///////////////////////
    /////////////////////////////////////////////////////////
    
    cvShowImage("Process Image Result",res);
}

void MainWindow::normalize(float &xp,float &yp, float &rp,float &xi, float &yi,float &ri ,IplImage *src){
    
    float xpupil;   // point on pupil x
    float ypupil;   // point on pupil y
    float xiris;    // point on iris x
    float yiris;    // point on iris y
    float xpa=0;
    float ypa=0;
    
    float theta = 0 ;  //col
    float K = 0.0;
    
    int N = 60;
    
    normalized    = cvCreateImage   (cvSize(360,N), IPL_DEPTH_8U, 3);
    greynormalized= cvCreateImage   (cvSize(360,N), IPL_DEPTH_8U, 1);
    eqnormalized  = cvCreateImage   (cvSize(360,N), IPL_DEPTH_8U, 1);
    
    for ( theta=0;theta<360;theta++) // col
    {
        // point on the pupil
        xpupil = (rp * cos(theta*(CV_PI/180)  )) + xp;
        ypupil = (rp * sin(theta*(CV_PI/180)  )) + yp;
        
        // point on the iris
        xiris =  (ri *cos(theta*(CV_PI/180))   ) + xi;
        yiris =  (ri * sin(theta*(CV_PI/180))  ) + yi;
        
        for(K=0;K<N;K++)                  //row
        {
            xpa=  ( (1-(((float)K+1)/(float)N) ) * (xp+ (rp*cos(theta*(CV_PI/180))))   ) + (  (((float)K+1)/(float)N) *  (xi+ (ri*cos(theta*(CV_PI/180))))  );
            ypa=  ( (1-(((float)K+1)/(float)N) ) * (yp+ (rp*sin(theta*(CV_PI/180))))  ) + (  (((float)K+1)/(float)N) *  (yi+ (ri*sin(theta*(CV_PI/180))))   );
            
            cvSet2D( normalized, K, theta ,cvGet2D(src,(ypa),(xpa)));    //Nomralized(theta,k)= image(xpa,ypa);
        }
        
    }
    
}

void MainWindow::on_ProcesImage_clicked()
{
    process_image();
}

void MainWindow::on_CreateGabor_clicked()
{
    normalize(xp, yp, rp, xi, yi,ri ,src);
    
    cvCvtColor(normalized,greynormalized,CV_RGB2GRAY);
    cvShowImage("Grey Normalized",greynormalized);
    
    cvEqualizeHist(greynormalized,eqnormalized);
    gabor.create_gabor_kernel_v1(0,45,8,1,0,1);
    
    cvFilter2D(eqnormalized,real_gabor, gabor.kernel_real);
    cvShowImage("Real Wavelet",real_gabor);
    
    cvFilter2D(eqnormalized,imag_gabor, gabor.kernel_imag);
    cvShowImage("Imag Wavelet",imag_gabor);
}

void MainWindow::quantize_gabor_image_real(){
    
    CvScalar  old_val;
    CvScalar   new_val;
    
    for(int i=0; i<60;i++){
        for(int j=0;j<360;j++){
            
            old_val =	cvGet2D(real_gabor,i,j);
            
            if (old_val.val[0]>=0)
                new_val.val[0]=1;
            else
                new_val.val[0]=0;
            
            cvSet2D(quantized_real,i,j,new_val);
            
        }}
    
    cvShowImage("Quantized Real",quantized_real);
}

void MainWindow::quantize_gabor_image_imag(){
    
    CvScalar   old_val;
    CvScalar   new_val;
    
    for(int i=0; i<60;i++){
        for(int j=0;j<360;j++){
            old_val =	cvGet2D(imag_gabor,i,j);
            
            if (old_val.val[0]>=0)
                new_val.val[0]=1;
            else
                new_val.val[0]=0;
            cvSet2D(quantized_imag,i,j,new_val);
        }}
    
    cvShowImage("Quantized Imag",quantized_imag);
}

void MainWindow::generate_code(){
    
    CvScalar qreal;
    CvScalar qimag;
    
    int step = 1;
    
    for(int i=0;i<60;i++){
        for(int j=0;j<360;j++){
            qreal = cvGet2D(quantized_real,i,j);
            qimag = cvGet2D(quantized_imag,i,j);
            
            if (qreal.val[0]==0 && qimag.val[0]==0)
            {
                cvSet2D(code,i,2*j,qreal);
                cvSet2D(code,i,2*j+step,qimag);
            }
            else if (qreal.val[0]==0 && qimag.val[0]==1)
            {
                cvSet2D(code,i,2*j,qreal);
                cvSet2D(code,i,2*j+step,qimag);
            }
            else if (qreal.val[0]==1 && qimag.val[0]==0)
            {
                cvSet2D(code,i,2*j,qreal);
                cvSet2D(code,i,2*j+step,qimag);
            }
            else if (qreal.val[0]==1 && qimag.val[0]==1)
            {
                cvSet2D(code,i,2*j,qreal);
                cvSet2D(code,i,2*j+step,qimag);
            }
        }
    }
}

void MainWindow::save_image_code(char *file_name ,IplImage* code){
    cvScale(code,scaled_code,255);
    cvSaveImage(file_name,scaled_code);
}

void MainWindow::on_QuantizeGabor_clicked()
{
    quantize_gabor_image_imag();
    quantize_gabor_image_real();
}

void MainWindow::on_GenerateAndSave_clicked()
{
    std::string filename = "default";
    if(ui->textEdit->toPlainText().length()>0){
        filename = ui->textEdit->toPlainText().toStdString();
    }
    filename += ".bmp";
    generate_code();
    cvScale(code,scaled_code,255);
    cvSaveImage(filename.c_str(),scaled_code);
    ui->textEdit->setText("FILE SAVED!");
    cvShowImage("This iriscode", scaled_code);
}

void MainWindow::on_Load_button_code_2_clicked()
{
    // Looking for a image name in a directory with browsing box
    QString Directory="capturas";
    QString fn = QFileDialog::getOpenFileName(this,"Choose a frame to download",Directory, "Images (*.png *.xpm *.jpg *.bmp)");
    
    code2 = cvLoadImage(fn.toStdString().c_str());
    
    Code2_mat= imread(fn.toStdString(), CV_LOAD_IMAGE_COLOR);
    code2_source = fn.toStdString().c_str();
    memcpy(Image_code2->bits(),Code2_mat.data,Code2_mat.rows*Code2_mat.cols*sizeof(uchar)*3);
    viewer_code2->update();
    
}

float MainWindow::compare_code(IplImage * temp1,IplImage * temp2){
    
    CvScalar val;
    int   number_of_ones = 0;
    float HD;
    int   N = 43200; //720x60
    
    cvXor(temp1,temp2,compare_result);
    
    for(int i=0; i<compare_result->height;i++){
        for(int j=0; j<compare_result->width;j++){
            val=	cvGet2D(compare_result,i,j);
            if(val.val[0]==1)
                number_of_ones+=1;
        }
    }
    
    HD = number_of_ones/N;
    
    return HD;
}


float MainWindow::compare_code(IplImage * temp1,  char * file_name){
    
    IplImage *gray_temp2 = cvCreateImage   (cvSize(720,60), IPL_DEPTH_8U, 1);
    IplImage *temp2      = cvCreateImage   (cvSize(720,60), IPL_DEPTH_8U, 1);
    float HD;
    IplImage *temp_scaled_code = cvLoadImage(file_name);
    
    cvCvtColor(temp_scaled_code,gray_temp2,CV_RGB2GRAY);  //loaded image is 3 channels , convert to 1
    
    cvScale(gray_temp2,temp2,(float)1/255);
    
    CvScalar val;
    float   number_of_ones = 0.0;
    
    float   N = 43200.0;
    
    cvXor(temp1,temp2,compare_result);
    
    for(int i=0; i<compare_result->height;i++){
        for(int j=0; j<compare_result->width;j++){
            
            
            val=	cvGet2D(compare_result,i,j);
            if(val.val[0]==1)
                number_of_ones+=1;
        }}
    
    
    HD = number_of_ones/N;
    
    return HD;
    
}

void MainWindow::on_HD_Calc_clicked()
{
    std::ostringstream ss;
    
    float HD = compare_code(code,(char *)code2_source.c_str());
    
    ss<<HD;
    ui->HD->setText(ss.str().c_str());
}
