#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "GL/glut.h"
#include <Gabor.h>
#include <cvgabor.h>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <rcdraw.h>

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include<math.h>

using namespace cv;
using namespace std;

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    
    explicit MainWindow(QWidget *parent = 0);
    
    ~MainWindow();
    
private:
    
    Ui::MainWindow *ui;
    
    // user viewers
    RCDraw *viewer_original;
    
    RCDraw *viewer_code2;
    
    //---------------------------------------------
    // Qimages associated to viewers
    QImage *Image_Source;
    
    QImage *Image_code2;
    
    
    
    //---------------------------------------------
    
    
    
    Mat  Original_Image, mat_original,Code2_mat;
    
    
    
    
public:
    
    IplImage *normalized=0;
    IplImage *greynormalized=0;
    IplImage *eqnormalized=0;
    
    IplImage *real_gabor=cvCreateImage   (cvSize(360,60), IPL_DEPTH_32F, 1);
    IplImage *imag_gabor=cvCreateImage   (cvSize(360,60), IPL_DEPTH_32F, 1);
    IplImage *mag_gabor =cvCreateImage   (cvSize(360,60), IPL_DEPTH_32F, 1);
    
    IplImage *quantized_real = cvCreateImage   (cvSize(360,60), IPL_DEPTH_32F, 1);
    IplImage *quantized_imag = cvCreateImage   (cvSize(360,60), IPL_DEPTH_32F, 1);
    IplImage *code            =cvCreateImage   (cvSize(720,60), IPL_DEPTH_8U, 1);
    IplImage * scaled_code    = cvCreateImage(cvSize(720,60), IPL_DEPTH_8U, 1);
    
    IplImage * code2 = cvCreateImage   (cvSize(720,60), IPL_DEPTH_8U, 1);
    std::string code2_source;
    
    Gabor gabor;
    
    IplImage * captured_iris =cvCreateImage(cvSize(640,480),IPL_DEPTH_8U, 3 );
    //For reasons of initalizing other vars in size we initialice this to a default iris photo
    IplImage *src    = cvLoadImage("Imagenes/4.bmp");
    
    IplImage *dst    = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 3 );
    
    
    IplImage *grayscale    = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    
    IplImage *smooth = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    IplImage *pupil  = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    IplImage *iris   = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    IplImage *pedge  = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    IplImage *iedge  = cvCreateImage( cvSize( src->width, src->height ), IPL_DEPTH_8U, 1 );
    
    
    IplImage *eyelid_mask  = cvCreateImage( cvGetSize(src), IPL_DEPTH_8U, 1 );
    IplImage *iris_mask    = cvCreateImage(cvGetSize(src), 8, 1);
    IplImage *mask         = cvCreateImage(cvGetSize(src), IPL_DEPTH_8U, 3 );
    IplImage *res          = cvCreateImage(cvGetSize(src), 8, 3);
    
    IplImage *compare_result = cvCreateImage(cvSize(720,60), IPL_DEPTH_8U, 1);
    
    int xroi,yroi;
    
    float xp=0;
    float yp=0;
    float rp=0;
    float xi=0;
    float yi=0;
    float ri=0;
    
    float shift = (float)(src->width/5);
    void process_image();
    IplImage* DrawHistogram(CvHistogram *hist, float scaleX, float scaleY);
    
    QImage * IplImage2QImage(IplImage *iplImg);
    IplImage* QImage2IplImage(QImage *qimg);
    
    void Smooth(IplImage *src , IplImage *dst);
    void normalize(float &xp,float &yp, float &rp,float &xi, float &yi,float &ri ,IplImage *src);
    
    void quantize_gabor_image_real();
    
    void quantize_gabor_image_imag();
    
    void generate_code();
    
    void save_image_code(char * file_name, IplImage * code);
    
    float compare_code(IplImage * temp1,IplImage * temp2);
    
    float compare_code(IplImage * temp1,  char * dst_temp2);
    
    
    public slots: // Connect to user buttons
    void Load_Image();
    
    private slots:
    void on_ProcesImage_clicked();
    
    void on_CreateGabor_clicked();
    void on_QuantizeGabor_clicked();
    void on_GenerateAndSave_clicked();
    void on_Load_button_code_2_clicked();
    void on_HD_Calc_clicked();
};

#endif // MAINWINDOW_H
