/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QFrame *viewer_original;
    QPushButton *Load_button;
    QLabel *label;
    QLabel *label_2;
    QPushButton *ProcesImage;
    QPushButton *CreateGabor;
    QPushButton *QuantizeGabor;
    QPushButton *GenerateAndSave;
    QLabel *label_3;
    QTextEdit *textEdit;
    QFrame *viewer_code_2;
    QPushButton *Load_button_code_2;
    QLabel *label_4;
    QLabel *HD;
    QPushButton *HD_Calc;

    void setupUi(QWidget *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(1200, 600);
        MainWindow->setMouseTracking(true);
        MainWindow->setAcceptDrops(true);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../../../Digital_books_and_other_things/TESIS_PACO/goeiti.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setWindowOpacity(1);
        viewer_original = new QFrame(MainWindow);
        viewer_original->setObjectName(QString::fromUtf8("viewer_original"));
        viewer_original->setEnabled(true);
        viewer_original->setGeometry(QRect(20, 30, 200, 200));
        viewer_original->setMaximumSize(QSize(600, 16777215));
        viewer_original->setBaseSize(QSize(400, 400));
        QPalette palette;
        QBrush brush(QColor(240, 29, 14, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        QBrush brush1(QColor(240, 240, 240, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        viewer_original->setPalette(palette);
        viewer_original->setFrameShape(QFrame::StyledPanel);
        viewer_original->setFrameShadow(QFrame::Raised);
        viewer_original->setLineWidth(10);
        Load_button = new QPushButton(MainWindow);
        Load_button->setObjectName(QString::fromUtf8("Load_button"));
        Load_button->setGeometry(QRect(60, 240, 121, 27));
        label = new QLabel(MainWindow);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(40, 10, 131, 17));
        label_2 = new QLabel(MainWindow);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(530, 40, 251, 20));
        ProcesImage = new QPushButton(MainWindow);
        ProcesImage->setObjectName(QString::fromUtf8("ProcesImage"));
        ProcesImage->setGeometry(QRect(60, 270, 121, 27));
        CreateGabor = new QPushButton(MainWindow);
        CreateGabor->setObjectName(QString::fromUtf8("CreateGabor"));
        CreateGabor->setGeometry(QRect(58, 300, 121, 27));
        QuantizeGabor = new QPushButton(MainWindow);
        QuantizeGabor->setObjectName(QString::fromUtf8("QuantizeGabor"));
        QuantizeGabor->setGeometry(QRect(60, 330, 121, 27));
        GenerateAndSave = new QPushButton(MainWindow);
        GenerateAndSave->setObjectName(QString::fromUtf8("GenerateAndSave"));
        GenerateAndSave->setGeometry(QRect(30, 420, 181, 41));
        label_3 = new QLabel(MainWindow);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(60, 370, 131, 20));
        textEdit = new QTextEdit(MainWindow);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(30, 390, 181, 31));
        viewer_code_2 = new QFrame(MainWindow);
        viewer_code_2->setObjectName(QString::fromUtf8("viewer_code_2"));
        viewer_code_2->setEnabled(true);
        viewer_code_2->setGeometry(QRect(240, 80, 720, 60));
        viewer_code_2->setMaximumSize(QSize(1000, 16777215));
        viewer_code_2->setBaseSize(QSize(200, 200));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Highlight, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Highlight, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::Highlight, brush1);
        viewer_code_2->setPalette(palette1);
        viewer_code_2->setFrameShape(QFrame::StyledPanel);
        viewer_code_2->setFrameShadow(QFrame::Raised);
        viewer_code_2->setLineWidth(10);
        Load_button_code_2 = new QPushButton(MainWindow);
        Load_button_code_2->setObjectName(QString::fromUtf8("Load_button_code_2"));
        Load_button_code_2->setGeometry(QRect(530, 170, 121, 27));
        label_4 = new QLabel(MainWindow);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(420, 310, 131, 17));
        HD = new QLabel(MainWindow);
        HD->setObjectName(QString::fromUtf8("HD"));
        HD->setGeometry(QRect(600, 310, 67, 16));
        HD_Calc = new QPushButton(MainWindow);
        HD_Calc->setObjectName(QString::fromUtf8("HD_Calc"));
        HD_Calc->setGeometry(QRect(500, 240, 151, 27));
        Load_button->raise();
        viewer_original->raise();
        label->raise();
        label_2->raise();
        ProcesImage->raise();
        CreateGabor->raise();
        QuantizeGabor->raise();
        GenerateAndSave->raise();
        label_3->raise();
        textEdit->raise();
        viewer_code_2->raise();
        Load_button_code_2->raise();
        label_4->raise();
        HD->raise();
        HD_Calc->raise();

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QWidget *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "IRIS", 0, QApplication::UnicodeUTF8));
        Load_button->setText(QApplication::translate("MainWindow", "Load  Image", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Create a new code", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Other iriscode", 0, QApplication::UnicodeUTF8));
        ProcesImage->setText(QApplication::translate("MainWindow", "Process image", 0, QApplication::UnicodeUTF8));
        CreateGabor->setText(QApplication::translate("MainWindow", "Create Gabor", 0, QApplication::UnicodeUTF8));
        QuantizeGabor->setText(QApplication::translate("MainWindow", "Quantize Gabor", 0, QApplication::UnicodeUTF8));
        GenerateAndSave->setText(QApplication::translate("MainWindow", "Generate and Save Code", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "Insert file name", 0, QApplication::UnicodeUTF8));
        Load_button_code_2->setText(QApplication::translate("MainWindow", "Load  Image", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "Hamming distance", 0, QApplication::UnicodeUTF8));
        HD->setText(QString());
        HD_Calc->setText(QApplication::translate("MainWindow", "Calculate Hamming", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
